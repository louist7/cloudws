var app = angular.module('app', []);
app.controller('flightController', ['$scope', '$http', function($scope, $http) {

        var baseURL = 'http://' + location.host;

        //CURRENCY CONVERSION
        var getCurrencies = function() {
            var request =
                    '<?xml version="1.0" encoding="utf-8"?>' +
                    '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"> ' +
                    '<SOAP-ENV:Header/> ' +
                    '<S:Body>' +
                    '<ns2:GetCurrencyCodes xmlns:ns2="http://DOCwebServices/"/>' +
                    '</S:Body>' +
                    '</S:Envelope>';

            $.ajax({
                type: "POST",
                url: baseURL + "/CurrencyConvertor/CurrencyConversionWSService",
                contentType: "text/xml",
                Type: "xml",
                data: request,
                success: processSuccess
            });

            function processSuccess(data, status, req) {
                if (status === "success") {
                    //transform xml response into json
                    var codes = x2js.xml_str2json(req.responseText);
                    var obj = codes.Envelope.Body_asArray[0];
                    var arr = obj.GetCurrencyCodesResponse.return;

                    //set currencies variable on scope
                    $scope.currencies = arr;
                }
            }
        };

        getCurrencies();

        var currency = 'GBP';
        $scope.updateCurrency = function(code) {
            
            var from = currency;
            var to = code.substring(0, 3);
            var request =
                    '<?xml version="1.0" encoding="utf-8"?>' +
                    '<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/" xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/"> ' +
                    '<SOAP-ENV:Header/> ' +
                    '<S:Body>' +
                    '<ns2:GetConversionRate xmlns:ns2="http://DOCwebServices/">' +
                    '<arg0>' + from + '</arg0>' +
                    '<arg1>' + to + '</arg1>' +
                    '</ns2:GetConversionRate>' +
                    '</S:Body>' +
                    '</S:Envelope>';

            $.ajax({
                type: "POST",
                url: baseURL + "/CurrencyConvertor/CurrencyConversionWSService",
                contentType: "text/xml",
                Type: "xml",
                data: request,
                success: processSuccess
            });

            function processSuccess(data, status, req) {
                if (status === "success") {
                    //transform xml response into json
                    var rates = x2js.xml_str2json(req.responseText);
                    var ratesArr = rates.Envelope.Body_asArray[0];
                    var rate = ratesArr.GetConversionRateResponse.return;

                    updateFaresInTable(rate, to);
                }
            }
            currency = to;
        };

        var updateFaresInTable = function(rate, code) {
            var flights = $scope.flights;
            for (var i = 0; i < flights.length; i++) {
                var flight = flights[i];
                flight.currency = code;
                flight.value = (flight.value * rate).toFixed(2);
            }
            $scope.$apply();
        };
        
        //SEARCH FLIGHTS
        $scope.flightSearch = {};

        $scope.searchFlights = function() {
            $http.post(baseURL + '/FlightBookingWS/webresources/booking/search', $scope.flightSearch).
                    success(function(data) {
                        $scope.flights = data;
                        //  do any currency conversions
                        var temp = currency;
                        currency = 'GBP';
                        $scope.updateCurrency(temp);
                    });
        };

        //    On Page Load search all flights;
        $scope.searchFlights();
        
        $scope.clearSearch = function() {
            $scope.flightSearch = {};
            $scope.searchFlights();
        };

        //BOOK FLIGHTS
        $scope.bookingDetails = {};

        $scope.bookFlight = function(flight) {
            $scope.notEnoughSeats = false;
            $scope.viewingMap = false;
            $scope.viewingBookings = false;
            $scope.viewingPics = false;
            $scope.amendBookingInProgress = false;
            $scope.bookingInProgress = true;
            
            $scope.bookingDetails.flightID = flight.flightID;
            $scope.bookingDetails.name = '';
            $scope.bookingDetails.numOfSeats = null;
            
            $scope.bookingDetails.price = flight.value;
            $scope.bookingDetails.totalCost = currency + '  ' + 0;
        };
        
        $scope.seatChange = function() {
            if ($scope.bookingDetails.numOfSeats) {
                var newValue = ($scope.bookingDetails.numOfSeats * $scope.bookingDetails.price).toFixed(2);
                $scope.bookingDetails.totalCost = currency + '  ' + newValue;
            } else {
                $scope.bookingDetails.totalCost = currency + '  ' + 0;
            }
        };

        $scope.makeBooking = function() {
            var id = $scope.bookingDetails.flightID;
            var name = $scope.bookingDetails.name;
            var num = $scope.bookingDetails.numOfSeats;

            $http.post(baseURL + '/FlightBookingWS/webresources/booking/bookFlight?flightID=' + id + '&name=' + name + '&numOfSeats=' + num).
                    success(function(data, status) {
                        if (status === 200) {
                            $scope.bookingInProgress = false;
                            $scope.searchFlights();
                        } else {
                            $scope.notEnoughSeats = true;
                        }
                    });
        };

        $scope.amendBooking = function(booking) {
            $scope.notEnoughSeats = false;
            $scope.viewingBookings = false;
            $scope.amendBookingInProgress = true;
            
            $scope.bookingDetails.flightID = $scope.flight.flightID;
            $scope.bookingDetails.bookingID = booking.bookingID;
            $scope.bookingDetails.name = booking.name;
            $scope.bookingDetails.numOfSeats = booking.numOfSeats;
            $scope.bookingDetails.price = $scope.flight.value;
            $scope.seatChange();
        };

        $scope.cancelBooking = function() {
            $scope.bookingInProgress = false;
            $scope.amendBookingInProgress = false;
        };

        $scope.viewAllBookings = function(flight) {
            $scope.bookingInProgress = false;
            $scope.viewingMap = false;
            $scope.viewingPics = false;
            $scope.amendBookingInProgress = false;
            $scope.viewingBookings = true;
            
            $scope.flight = flight;
            $http.get(baseURL + '/FlightBookingWS/webresources/booking/viewAll?flightID=' + flight.flightID).
                    success(function(data) {
                        if (data.length > 0) {
                            $scope.noBookings = false;
                            $scope.hasBookings = true;
                            $scope.bookingsForFlight = data;
                        } else {
                            $scope.noBookings = true;
                            $scope.hasBookings = false;
                        }
                        
                    });
        };
        
        $scope.cancel = function(booking) {
            $http.post(baseURL + '/FlightBookingWS/webresources/booking/cancelBooking?flightID=' + $scope.flight.flightID + '&bookingID=' + booking.bookingID).
                    success(function(data) {
                        $scope.searchFlights();
                        $scope.viewAllBookings($scope.flight);
                    });
        };

        $scope.makeBookingAmendment = function() {
            $http.post(baseURL + '/FlightBookingWS/webresources/booking/amendBooking?flightID=' +
                    $scope.flight.flightID + '&bookingID=' + $scope.bookingDetails.bookingID + 
                    '&name=' + $scope.bookingDetails.name + '&numOfSeats=' + $scope.bookingDetails.numOfSeats).
                    success(function(data, status) {
                        if (status === 200) {
                            $scope.notEnoughSeats = false;
                            $scope.searchFlights();
                            $scope.viewAllBookings($scope.flight);
                        } else {
                            $scope.notEnoughSeats = true;
                        }       
                    });
        };
        //MAPS API
        $scope.renderMap = function(flight) {
            $scope.viewingBookings = false;
            $scope.bookingInProgress = false;
            $scope.viewingPics = false;
            $scope.amendBookingInProgress = false;
            $scope.viewingMap = true;

            var directionsService = new google.maps.DirectionsService;

            var map = new google.maps.Map(document.getElementById('map'), {});

            var directionsDisplay = new google.maps.DirectionsRenderer();
            
            directionsDisplay.setMap(map);
            directionsDisplay.setPanel(document.getElementById('directions'));

            directionsService.route({
                origin: flight.originCity,
                destination: flight.destinationCity,
                travelMode: google.maps.TravelMode.DRIVING
            }, function(response, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                } else {
                    window.alert('Request failed due to ' + status);
                }
            });
        };
        
        //PANORAMIO API
        $scope.showPics = function(city) {
            $scope.viewingBookings = false;
            $scope.bookingInProgress = false;
            $scope.viewingMap = false;
            $scope.amendBookingInProgress = false;
            $scope.viewingPics = true;
            
            var myRequest = new panoramio.PhotoRequest({
                'tag': city
            });
            
            var myOptions = {
                'width': 500,
                'height': 325
            };
            
            var wapiblock = document.getElementById('wapiblock');
            var widget = new panoramio.PhotoWidget(wapiblock, myRequest, myOptions);
            widget.setPosition(0); 
        };
    }]);