package org.data;

import java.util.List;
import org.bindings.AvailableFlights;
import org.bindings.BookingDetails;
import org.service.FlightPOJO;

public interface FlightDao {
    AvailableFlights searchAvailableFlights(FlightPOJO constraints);
    
    Boolean makeBooking(String flightID, String name, int numOfSeats);
    
    List<BookingDetails> getBookings(String flightID);
    
    Boolean cancelBooking(String flightID, String bookingID);
    
    Boolean amendBooking(String flightID, String bookingID, String name, int numOfSeats);
}
