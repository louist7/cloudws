package org.data;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import org.bindings.AvailableFlights;
import org.bindings.BookingDetails;
import org.bindings.FlightDetails;
import org.service.FlightPOJO;
import org.springframework.util.StringUtils;

public class FlightDaoImpl implements FlightDao {
    
    private AvailableFlights unmarshalFlightDetails() {
        
        AvailableFlights flights = new AvailableFlights();
        try {
            javax.xml.bind.JAXBContext jaxbCtx = javax.xml.bind.JAXBContext.newInstance(flights.getClass().getPackage().getName());
            javax.xml.bind.Unmarshaller unmarshaller = jaxbCtx.createUnmarshaller();
            
            URL resourceURL = this.getClass().getResource("availableFlights.xml");
            URLConnection resourceConn = resourceURL.openConnection();
            resourceConn.setUseCaches(false);
            InputStream flightsXML = resourceConn.getInputStream();
            
            flights = (AvailableFlights) unmarshaller.unmarshal(flightsXML);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return flights;
    }
    
    private void marshalFlightDetails(AvailableFlights availableFlights) {
        
        try {            
            javax.xml.bind.JAXBContext jaxbCtx = javax.xml.bind.JAXBContext.newInstance(availableFlights.getClass().getPackage().getName());
            javax.xml.bind.Marshaller marshaller = jaxbCtx.createMarshaller();
            marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_ENCODING, "UTF-8"); //NOI18N
            marshaller.setProperty(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
                    
            URL resourceUrl = this.getClass().getResource("availableFlights.xml");
            File flightsXML = new File(resourceUrl.toURI());
            
            marshaller.marshal(availableFlights, flightsXML);
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    @Override
    public AvailableFlights searchAvailableFlights(FlightPOJO constraints) {
        if (constraints.isMatchAllCriteria() != null) {
            if(constraints.isMatchAllCriteria()) {
                return matchAllCriteria(constraints);
            }
        }
        return matchPartialCriteria(constraints);
    }
    
    private AvailableFlights matchAllCriteria(FlightPOJO constraints) {
        AvailableFlights availableFlights = new AvailableFlights();
                
        for (FlightDetails flight : unmarshalFlightDetails().getFlightCollection()) {
            Boolean beingAdded = true;
            // check origin city
            if (!StringUtils.isEmpty(constraints.getOriginCity())) {
                String flightOrigin = flight.getOriginCity().toLowerCase();
                String queryOrigin = constraints.getOriginCity().toLowerCase();
                if (!flightOrigin.contains(queryOrigin)) {
                    beingAdded = false;
                }
            }
            // check destination city
            if (!StringUtils.isEmpty(constraints.getDestinationCity())) {
                String flightDestination = flight.getDestinationCity().toLowerCase();
                String queryDestination = constraints.getDestinationCity().toLowerCase();
                if (!flightDestination.contains(queryDestination)) {
                    beingAdded = false;
                }
            }
            // check airline
            if (!StringUtils.isEmpty(constraints.getAirline())) {
                String flightAirline = flight.getAirline().toLowerCase();
                String queryAirline = constraints.getAirline().toLowerCase();
                if (!flightAirline.contains(queryAirline)) {
                    beingAdded = false;
                }
            }
            //check date
            if (constraints.getDate() != null) {
                if (!flight.getDate().equals(constraints.getDate())) {
                    beingAdded = false;
                }
            }
            //check connections
            if (constraints.isDirect() != null) {
                if (constraints.isDirect() && flight.getNumberOfConnections() != 0) {
                    beingAdded = false;
                }
            }
            
            if (beingAdded) {
                availableFlights.getFlightCollection().add(flight);
            }
        }
        return availableFlights;
    }
    
    private AvailableFlights matchPartialCriteria(FlightPOJO constraints) {
        AvailableFlights availableFlights = new AvailableFlights();
        for (FlightDetails flight : unmarshalFlightDetails().getFlightCollection()) {
            Boolean allNull = true;
            // check origin city
            if (!StringUtils.isEmpty(constraints.getOriginCity())) {
                allNull = false;
                String flightOrigin = flight.getOriginCity().toLowerCase();
                String queryOrigin = constraints.getOriginCity().toLowerCase();
                if (flightOrigin.contains(queryOrigin)) {
                    availableFlights.getFlightCollection().add(flight);
                    continue;
                }
            }
            // check destination city
            if (!StringUtils.isEmpty(constraints.getDestinationCity())) {
                allNull = false;
                String flightDestination = flight.getDestinationCity().toLowerCase();
                String queryDestination = constraints.getDestinationCity().toLowerCase();
                if (flightDestination.contains(queryDestination)) {
                    availableFlights.getFlightCollection().add(flight);
                    continue;
                }
            }
            // check airline
            if (!StringUtils.isEmpty(constraints.getAirline())) {
                allNull = false;
                String flightAirline = flight.getAirline().toLowerCase();
                String queryAirline = constraints.getAirline().toLowerCase();
                if (flightAirline.contains(queryAirline)) {
                    availableFlights.getFlightCollection().add(flight);
                    continue;
                }
            }
            //check date
            if (constraints.getDate() != null) {
                allNull = false;
                if (flight.getDate().equals(constraints.getDate())) {
                    availableFlights.getFlightCollection().add(flight);
                    continue;
                }
            }
            //check connections
            if (constraints.isDirect() != null) {
                allNull = false;
                if (constraints.isDirect() && flight.getNumberOfConnections() == 0) {
                    availableFlights.getFlightCollection().add(flight);
                    continue;
                }
            }
            
            if (allNull) {
                //all search criteria is null so add the flight to the list.
                availableFlights.getFlightCollection().add(flight);
            }
        }
        return availableFlights;
    }
    @Override
    public Boolean makeBooking(String flightID, String name, int numOfSeats) {
        
        AvailableFlights availableFlights = unmarshalFlightDetails();
        
        for (FlightDetails flight : availableFlights.getFlightCollection()) {
            if (flight.getFlightID().equals(flightID)) {
                int seatsRemaining = flight.getAvailableSeats() - numOfSeats;
                if (seatsRemaining < 0) {
                    return false;
                } else {
                    //make booking
                    int pos = flight.getBookings().size() + 1;
                    BookingDetails bookingDetails = new BookingDetails();
                    bookingDetails.setBookingID(String.valueOf(pos));
                    bookingDetails.setName(name);
                    bookingDetails.setNumOfSeats(numOfSeats);
                    bookingDetails.setCancelled(false);
                    flight.getBookings().add(bookingDetails);
                    flight.setAvailableSeats(seatsRemaining);
                }
            }
        }
        marshalFlightDetails(availableFlights);
        return true;
    }
    
    @Override
    public List<BookingDetails> getBookings(String flightID) {
        List<BookingDetails> bookings = new ArrayList<BookingDetails>();
        
        AvailableFlights availableFlights = unmarshalFlightDetails();
        
        for (FlightDetails flight : availableFlights.getFlightCollection()) {
             if (flight.getFlightID().equals(flightID)) {
                 bookings = flight.getBookings();
             }
        }
        return bookings;
    }
    
    @Override
    public Boolean cancelBooking(String flightID, String bookingID) {
        AvailableFlights availableFlights = unmarshalFlightDetails();
        
        for (FlightDetails flight : availableFlights.getFlightCollection()) {
             if (flight.getFlightID().equals(flightID)) {
                 for (BookingDetails booking : flight.getBookings()) {
                     if (booking.getBookingID().equals(bookingID)) {
                         int seats = flight.getAvailableSeats();
                         flight.setAvailableSeats(seats + booking.getNumOfSeats());
                         booking.setCancelled(true);
                     }
                 }
             }
        }
        marshalFlightDetails(availableFlights);
        return true;
    }
    
    @Override
    public Boolean amendBooking(String flightID, String bookingID, String name, int numOfSeats) {
        
        AvailableFlights availableFlights = unmarshalFlightDetails();
        for (FlightDetails flight : availableFlights.getFlightCollection()) {
            if (flight.getFlightID().equals(flightID)) {
                for (BookingDetails booking : flight.getBookings()) {
                    if (booking.getBookingID().equals(bookingID)) {
                        //add original seats back onto flight
                        int seats = flight.getAvailableSeats();
                        flight.setAvailableSeats(seats + booking.getNumOfSeats());
                        
                        //now remove the new number and make booking
                        int seatsRemaining = flight.getAvailableSeats() - numOfSeats;
                        if (seatsRemaining < 0) {
                            return false;
                        }
                        booking.setName(name);
                        booking.setNumOfSeats(numOfSeats);
                        flight.setAvailableSeats(seatsRemaining);
                    }
                }
            }
        }
        marshalFlightDetails(availableFlights);
        return true;
    }
}
