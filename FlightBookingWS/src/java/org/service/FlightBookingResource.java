package org.service;

import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import org.bindings.AvailableFlights;
import org.bindings.BookingDetails;
import org.bindings.FlightDetails;
import org.data.FlightDao;
import org.data.FlightDaoImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;

@Path("booking")
public class FlightBookingResource {

    private final FlightDao flightDao = new FlightDaoImpl();
    public FlightBookingResource() {
    }
    
    @POST
    @Path("/search")
    public Response search(@RequestBody FlightPOJO constraints) throws JSONException {

        AvailableFlights availableFlights = flightDao.searchAvailableFlights(constraints);

        JSONArray flights = new JSONArray();

        for (FlightDetails flight : availableFlights.getFlightCollection()) {

            JSONObject flightObj = new JSONObject();
            flightObj.put("flightID", flight.getFlightID());
            flightObj.put("originCity", flight.getOriginCity());
            flightObj.put("destinationCity", flight.getDestinationCity());
            flightObj.put("airline", flight.getAirline());
            flightObj.put("date", flight.getDate());
            flightObj.put("availableSeats", flight.getAvailableSeats());
            flightObj.put("numberOfConnections", flight.getNumberOfConnections());
            flightObj.put("currency", flight.getFare().getCurrency());
            flightObj.put("value", flight.getFare().getValue());

            flights.put(flightObj);
        }
        return Response.ok(flights.toString()).build();
    }

    @POST
    @Path("/bookFlight")
    public Response bookFlight(@QueryParam("flightID") String flightID, @QueryParam("name") String name, @QueryParam("numOfSeats") int seats) {

        if (flightDao.makeBooking(flightID, name, seats)) {
            return Response.ok().build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("/viewAll")
    public Response getAllBookings(@QueryParam("flightID") String flightID) throws JSONException {
        List<BookingDetails> bookingDetails = flightDao.getBookings(flightID);

        JSONArray bookings = new JSONArray();
        for (BookingDetails booking : bookingDetails) {
            if (!booking.isCancelled()) {
                JSONObject bookingObj = new JSONObject();

                bookingObj.put("bookingID", booking.getBookingID());
                bookingObj.put("name", booking.getName());
                bookingObj.put("numOfSeats", booking.getNumOfSeats());

                bookings.put(bookingObj);
            }
        }
        return Response.ok(bookings.toString()).build();
    }
    
    @POST
    @Path("/cancelBooking")
    public Response cancelBooking(@QueryParam("flightID") String flightID, @QueryParam("bookingID") String bookingID) {
        if (flightDao.cancelBooking(flightID, bookingID)) {
            return Response.ok().build();
        } else {
            return Response.noContent().build();
        }
    }
    
    @POST
    @Path("/amendBooking")
    public Response amendBooking(@QueryParam("flightID") String flightID, @QueryParam("bookingID") String bookingID,
                                @QueryParam("name") String name, @QueryParam("numOfSeats") int seats) {
        
        if (flightDao.amendBooking(flightID, bookingID, name, seats)) {
            return Response.ok().build();
        } else {
            return Response.noContent().build();
        }
    }
}
