package org.service;

import javax.xml.datatype.XMLGregorianCalendar;

public class FlightPOJO {
    private Boolean matchAllCriteria;
    private String flightID;
    private String originCity;
    private String destinationCity;
    private XMLGregorianCalendar date;
    private String airline;
    private int availableSeats;
    private Boolean direct;

    public Boolean isMatchAllCriteria() {
        return matchAllCriteria;
    }

    public void setMatchAllCriteria(Boolean matchAllCriteria) {
        this.matchAllCriteria = matchAllCriteria;
    }
    
    public String getFlightID() {
        return flightID;
    }

    public void setFlightID(String flightID) {
        this.flightID = flightID;
    }
    
    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public XMLGregorianCalendar getDate() {
        return date;
    }

    public void setDate(XMLGregorianCalendar date) {
        this.date = date;
    }

    public String getAirline() {
        return airline;
    }

    public void setAirline(String airline) {
        this.airline = airline;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }
    
    public Boolean isDirect() {
        return direct;
    }

    public void setDirect(Boolean direct) {
        this.direct = direct;
    }
}
